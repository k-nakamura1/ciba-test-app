import NotificationCenter

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) { (granted, _) in
                NSLog("[Check] Permission granted: \(granted)")
            }
        
        let acceptAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
              title: "許可",
              options: UNNotificationActionOptions(rawValue: 0))
        let declineAction = UNNotificationAction(identifier: "DECLINE_ACTION",
              title: "不許可",
              options: UNNotificationActionOptions(rawValue: 0))
        
        let testCategory =
              UNNotificationCategory(identifier: "TEST_CATEGORY",
              actions: [acceptAction, declineAction],
              intentIdentifiers: [],
              hiddenPreviewsBodyPlaceholder: "",
              options: .customDismissAction)
        
        UNUserNotificationCenter.current().setNotificationCategories([testCategory])
        UNUserNotificationCenter.current().delegate = self
        return true
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
                
        let userInfo = response.notification.request.content.userInfo
        let authorization = userInfo["AUTHORIZATION"] as! String
        
        switch response.actionIdentifier {
            case "ACCEPT_ACTION":
                guard let url = URL(string: "http://localhost:8088/auth/realms/SampleRealm/protocol/openid-connect/ext/ciba/auth/callback") else { return }
                let data: [String: String] = ["status": "SUCCEED"]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: data, options: []) else { return }

                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(authorization, forHTTPHeaderField: "Authorization")
                request.httpBody = httpBody
                
                let task: URLSessionTask = URLSession.shared.dataTask(with: request, completionHandler: {(data, response, error) in
                    
                    if let error = error {
                        NSLog("Error: \(error)")
                        return;
                    }
                    
                    if let response = response as? HTTPURLResponse {
                        if response.statusCode != 200 {
                            NSLog("Status code: \(response.statusCode)")
                            return
                        }
                    }
                })
                task.resume()
                break
            case "DECLINE_ACTION":
                break
           default:
              break
        }

        completionHandler()
    }
}
